package test;

import java.util.ArrayList;
import java.util.List;

import football.FPosition;
import shared.DFSClassifier;
import shared.FlexPosition;
import shared.Player;
import shared.Position;
import shared.Stat;

public class TestClassifier extends DFSClassifier {
	private static final int SALARY = 100;
	private static final ArrayList<Position> rosterPositions = new ArrayList<Position>();
	private static final ArrayList<FlexPosition> rosterFlexPositions = new ArrayList<FlexPosition>();
	static {
		rosterPositions.add(FPosition.QB);
		rosterPositions.add(FPosition.RB);
		rosterPositions.add(FPosition.WR);
		
		rosterFlexPositions.add(new FlexPosition(FPosition.RB, FPosition.WR));
	}
	@Override public int getSalary() {
		return SALARY;
	}
	@Override public ArrayList<Position> getRosterPositions() {
		return rosterPositions;
	}
	@Override public ArrayList<FlexPosition> getRosterFlexPositions() {
		return rosterFlexPositions;
	}
	@Override public ArrayList<Player> loadPlayers() {
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(new Player(0, "Player 0", FPosition.QB, 50, 30));
		players.add(new Player(1, "Player 1", FPosition.QB, 40, 26));
		players.add(new Player(2, "Player 2", FPosition.QB, 34, 24));
		players.add(new Player(3, "Player 3", FPosition.RB, 25, 30));
		players.add(new Player(4, "Player 4", FPosition.RB, 19, 25));
		players.add(new Player(5, "Player 5", FPosition.RB, 15, 21));
		players.add(new Player(6, "Player 6", FPosition.WR, 25, 34));
		players.add(new Player(7, "Player 7", FPosition.WR, 22, 26));
		players.add(new Player(8, "Player 8", FPosition.WR, 13, 15));
		return players;
	}
	@Override public int getMinCostForPosition(Position position) {
		if (position == FPosition.QB) return 30;
		if (position == FPosition.RB) return 15;
		if (position == FPosition.WR) return 10;
		return 0;
	}
	@Override public double getPtsForStat(Stat stat, double value) {
		return 0;
	}
}
