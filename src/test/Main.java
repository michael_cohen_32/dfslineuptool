package test;

import football.draftkingsFB.DKFootballClassifier;
import geneticAlg.DFStoGA;
import shared.DFSClassifier;

public class Main {
	//entry point
	public static void main(String[] args) {
		DFSClassifier dfsClassifier = new DKFootballClassifier();
		DFStoGA ga = new DFStoGA(dfsClassifier);
		System.out.println(ga.getBestLineup());
		/*
		List<Integer> lockedInPlayers = Arrays.asList(1, 74, 75, 76);
		new LineupGenerator(dfsClassifier, lockedInPlayers, Arrays.asList());
		*/
	}
}
