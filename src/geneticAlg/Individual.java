package geneticAlg;

//represents an individual for the genetic algorithm (a lineup)
public class Individual {
	private Byte[] genes;
	private double fitness = -1;
	
	private FitnessCalculator fitnessCalculator;
	
	public Individual(int geneLength, FitnessCalculator fitnessCalculator) {
		genes = new Byte[geneLength];
		this.fitnessCalculator = fitnessCalculator;
	}
	public void fillInGenes() {
		for (int i = 0; i < size(); i++) {
			Byte gene = Byte.valueOf((byte) Math.round(Math.random()));
			genes[i] = gene;
		}
	}
	public Byte getGene(int idx) {
		return idx >= 0 && idx < genes.length ? genes[idx] : null;
	}
	public void setGene(int idx, Byte value) {
		if (idx >= 0 && idx < genes.length) genes[idx] = value; 
	}
	public int size() {
		return genes.length;
	}
	public double getFitness() {
		fitness = fitness == -1 ? fitnessCalculator.getFitness(this) : fitness;
		return fitness;
	}
	@Override public String toString() {
        String geneString = "";
        for (int i = 0; i < size(); i++) {
            geneString += getGene(i);
        }
        return geneString;
    }
}
