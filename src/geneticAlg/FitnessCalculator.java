package geneticAlg;

public abstract class FitnessCalculator {
	/** fitness function **/
	public abstract double getFitness(Individual individual);
	/** minimum fitness required of an individual to stop the algorithm **/
	public abstract double getFitnessCutoff();
}
