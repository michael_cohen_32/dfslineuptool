package geneticAlg;

//represents a population for the genetic algorithm (set of lineups)
public class Population {
	private Individual[] individuals;
	private int geneLength;
	private FitnessCalculator fitnessCalculator;
	
	/**
	 * @param populationSize
	 * @param geneLength number of bytes for individual
	 * @param init true if the individuals should be initialized
	 */
	public Population(int populationSize, int geneLength, FitnessCalculator fitnessCalculator, boolean init) {
		individuals = new Individual[populationSize];
		this.fitnessCalculator = fitnessCalculator;

		if (init) {
			for (int i = 0; i < size(); i++) {
				Individual ind = new Individual(geneLength, fitnessCalculator);
				ind.fillInGenes();
				saveIndividual(i, ind);
			}
		}
	}
	public Individual getIndividual(int idx) {
		return idx >= 0 && idx < individuals.length ? individuals[idx] : null;
	}
	public Individual getFittest() {
		if (individuals.length == 0) return null;
		Individual fittest = individuals[0];
		double fittestFitness = fittest.getFitness();
		for (int i = 1; i < size(); i++) {
			Individual nextInd = getIndividual(i);
			if (nextInd != null && nextInd.getFitness() >= fittestFitness) {
				fittest = nextInd; fittestFitness = nextInd.getFitness();
			}
		}
		return fittest;
	}
	public int size() {
		return individuals.length;
	}
	public void saveIndividual(int idx, Individual ind) {
		if (idx >= 0 && idx < size()) individuals[idx] = ind;
	}
}
