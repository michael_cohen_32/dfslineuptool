package geneticAlg;

import football.FProjections;
import shared.DFSClassifier;
import shared.FlexPosition;
import shared.Lineup;
import shared.Player;
import shared.Position;
import shared.Utils;

public class DFStoGA {
	private DFSClassifier dfsClassifier;
	private int minPtsForSolution;
	private int bytesPerPlayer;
	
	private FProjections projections;

	public DFStoGA(DFSClassifier dfsClassifier) {
		this.dfsClassifier = dfsClassifier; projections = new FProjections(dfsClassifier);
		int numBitsPerIndividual = 0;
		for (Position position : dfsClassifier.getRosterPositions()) {
			int numPlayers = dfsClassifier.getNumPlayersForPosition(position);
			numBitsPerIndividual += (int)Math.ceil(Math.log10(numPlayers) / Math.log10(2));
		}
		for (FlexPosition flexPosition : dfsClassifier.getRosterFlexPositions()) {
			int numPlayers = 0;
			for (Position position : flexPosition.getValidPositions()) {
				numPlayers += dfsClassifier.getNumPlayersForPosition(position);
			}
			numBitsPerIndividual += (int)Math.ceil(Math.log10(numPlayers) / Math.log10(2));
		}
		bytesPerPlayer = numBitsPerIndividual;
	}
	public DFStoGA(DFSClassifier dfsClassifier, int minPtsForSolution) {
		this.dfsClassifier = dfsClassifier; this.minPtsForSolution = minPtsForSolution; projections = new FProjections(dfsClassifier);
		bytesPerPlayer = (int)Math.ceil(Math.log10(dfsClassifier.getNumPlayers()) / Math.log10(2));
		/*
					log[10]x
		log[2]x = ----------
        			log[10]2
        */
	}
	public Lineup getBestLineup() {
		GA ga = getGAInstance();
		Individual fittest = ga.getFittest();
		return convertToLineup(fittest);
	}
	private GA getGAInstance() {
		return new GA() {
			@Override public double calculateFitness(Individual individual) {
				Lineup lineup = convertToLineup(individual);
				if (Utils.isValidLineup(lineup, dfsClassifier, true)) {
					int projectedPts = 0;
					for (Player player : lineup.getPlayers()) {
						projectedPts += projections.getProjectedPoints(player.getName());
					}
					return projectedPts;
				}
				return 0;
				//return Utils.isValidLineup(lineup, dfsClassifier, true) ? lineup.getProjectedPoints() : 0;
			}
			@Override public double calculateFitnessCutoff() {
				return minPtsForSolution;
			}
			@Override public int getGeneLength() {
				return bytesPerPlayer * dfsClassifier.getRosterSize();
			}
		};
	}
	private Lineup convertToLineup(Individual ind) {
		Lineup lineup = new Lineup();
		Byte[] bits = new Byte[bytesPerPlayer];
		for (int i = 0; i < ind.size(); i++) {
			if (i != 0 && i % bytesPerPlayer == 0) {
				int iPlayer = bitsToInteger(bits);
				Player player = dfsClassifier.getPlayer(iPlayer);
				if (player != null) lineup.addPlayer(player);
				bits = new Byte[bytesPerPlayer];
			}
			bits[i%bytesPerPlayer] = ind.getGene(i);
		}
		return lineup;
	}
	/**
	 * Each byte in "bits" is either 0 or 1...convert to int (i.e [1, 0] -> 2, [1, 0, 1, 1] -> 11)
	 */
	private static int bitsToInteger(Byte[] bits) {
		int ret = 0;
		for (Byte b : bits) {
			int bVal = b.intValue() == 1 ? 1 : 0;
			ret = ret*2 + bVal;
		}
		return ret;
	}
}
