package geneticAlg;

public abstract class GA {
	private static final int INITIAL_POP_SIZE = 50;
	public GA() {}
	public Individual getFittest() {
		FitnessCalculator fitnessCalc = new FitnessCalculator() {
			@Override public double getFitness(Individual individual) {
				return calculateFitness(individual);
			}
			@Override public double getFitnessCutoff() {
				return calculateFitnessCutoff();
			}
		};
		Algorithm alg = new Algorithm(getGeneLength(), fitnessCalc);
		Population population = new Population(INITIAL_POP_SIZE, getGeneLength(), fitnessCalc, true);
		int generationCount = 0;
		while (population.getFittest().getFitness() < fitnessCalc.getFitnessCutoff()) {
			generationCount++;
			System.out.println("Generation: " + generationCount + " Fittest: " + population.getFittest().getFitness());
            population = alg.evolvePopulation(population);
		}
		return population.getFittest();
	}
	public abstract double calculateFitness(Individual individual);
	public abstract double calculateFitnessCutoff();
	public abstract int getGeneLength();
}
