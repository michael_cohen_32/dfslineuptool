package geneticAlg;

import java.util.ArrayList;
import java.util.List;

import football.FProjections;
import shared.DFSClassifier;
import shared.FlexPosition;
import shared.Lineup;
import shared.Player;
import shared.Position;
import shared.Utils;

public class DFStoGA2 {
	private DFSClassifier dfsClassifier;
	private int bitsPerIndividual;
	private List<Integer> bitLengths;
	private FProjections projections;

	public DFStoGA2(DFSClassifier dfsClassifier) {
		this.dfsClassifier = dfsClassifier; projections = new FProjections(dfsClassifier);
		bitsPerIndividual = 0; bitLengths = new ArrayList<Integer>();
		for (Position position : dfsClassifier.getRosterPositions()) {
			int numPlayers = dfsClassifier.getNumPlayersForPosition(position);
			int bits = (int)Math.ceil(Math.log10(numPlayers) / Math.log10(2));
			bitsPerIndividual += bits;
			bitLengths.add(bits);
		}
		for (FlexPosition flexPosition : dfsClassifier.getRosterFlexPositions()) {
			int numPlayers = 0;
			for (Position position : flexPosition.getValidPositions()) {
				numPlayers += dfsClassifier.getNumPlayersForPosition(position);
			}
			int bits = (int)Math.ceil(Math.log10(numPlayers) / Math.log10(2));
			bitsPerIndividual += bits;
			bitLengths.add(bits);
		}
	}
	public Lineup getBestLineup() {
		GA ga = getGAInstance();
		Individual fittest = ga.getFittest();
		return convertToLineup(fittest);
	}
	private GA getGAInstance() {
		return new GA() {
			@Override public double calculateFitness(Individual individual) {
				Lineup lineup = convertToLineup(individual);
				if (lineup == null) return 0;
				if (lineup.getCost() > dfsClassifier.getSalary()) return 0;
				double projPts = getProjectedPoints(lineup);
				//System.out.println("proj pts: " + projPts);
				return projPts;
			}
			@Override public double calculateFitnessCutoff() {
				return 150;
			}
			@Override public int getGeneLength() {
				return bitsPerIndividual;
			}
		};
	}
	private double getProjectedPoints(Lineup lineup) {
		double projPts = 0;
		for (Player player : lineup.getPlayers()) {
			projPts += projections.getProjectedPoints(player.getName());
		}
		return projPts;
	}
	private Lineup convertToLineup(Individual ind) {
		Lineup lineup = new Lineup();
		List<Position> rosterPositions = dfsClassifier.getRosterPositions();
		int idx = 0;
		for (int i = 0; i < rosterPositions.size(); i++) {
			int numBits = bitLengths.get(i);
			Byte[] playerBits = new Byte[numBits];
			for (int j = 0; j < numBits; j++) {
				playerBits[j] = ind.getGene(idx);
				idx++;
			}
			int iPlayer = bitsToInteger(playerBits);
			Player player = dfsClassifier.getPlayerAtPositionIndex(iPlayer, rosterPositions.get(i));
			if (player == null) return null;
			lineup.addPlayer(player);
		}
		List<FlexPosition> rosterFlexPositions = dfsClassifier.getRosterFlexPositions();
		for (int i = 0; i < rosterFlexPositions.size(); i++) {
			int numBits = bitLengths.get(rosterPositions.size() + i);
			Byte[] playerBits = new Byte[numBits];
			for (int j = 0; j < numBits; j++) {
				playerBits[j] = ind.getGene(idx);
				idx++;
			}
			int iPlayer = bitsToInteger(playerBits);
			Player player = dfsClassifier.getPlayerAtFlexPositionIndex(iPlayer, rosterFlexPositions.get(i));
			if (player == null) return null;
			lineup.addPlayer(player);
		}
		return lineup;
	}
	/**
	 * Each byte in "bits" is either 0 or 1...convert to int (i.e [1, 0] -> 2, [1, 0, 1, 1] -> 11)
	 */
	private static int bitsToInteger(Byte[] bits) {
		int ret = 0;
		for (Byte b : bits) {
			int bVal = b.intValue() == 1 ? 1 : 0;
			ret = ret*2 + bVal;
		}
		return ret;
	}
}