package football.draftkingsFB;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import football.FPosition;
import football.FStat;
import shared.DFSClassifier;
import shared.FlexPosition;
import shared.Player;
import shared.Position;
import shared.Stat;

public class DKFootballClassifier extends DFSClassifier {
	private static final int WEEK = 7;
	private static final String PATH = "/Users/Michael/Documents/dfs_data/Salaries/Week" + WEEK + ".csv";
	private static final int SALARY = 50000;
	private static final ArrayList<Position> rosterPositions = new ArrayList<Position>();
	static {
		rosterPositions.add(FPosition.QB);
		rosterPositions.add(FPosition.RB);
		rosterPositions.add(FPosition.RB);
		rosterPositions.add(FPosition.WR);
		rosterPositions.add(FPosition.WR);
		rosterPositions.add(FPosition.WR);
		rosterPositions.add(FPosition.TE);
		rosterPositions.add(FPosition.DST);
	}
	private static final ArrayList<FlexPosition> rosterFlexPositions = new ArrayList<FlexPosition>();
	static {
		rosterFlexPositions.add(new FlexPosition(FPosition.RB, FPosition.WR, FPosition.TE));
	}
	
	public DKFootballClassifier() {
		super();
	}
	
	public ArrayList<Player> loadPlayers() {
		ArrayList<Player> players = new ArrayList<Player>();
		BufferedReader br = null; String line = ""; String csvSplitBy =",";
		try {
			br = new BufferedReader(new FileReader(PATH));
			boolean first = true; int iPlayer = 0;
			while ((line = br.readLine()) != null) {
				if (first) {
					first = false;
					continue;
				} else {
					line = line.replaceAll("\"", "");
					String[] playerFields = line.split(csvSplitBy);
					Position position = FPosition.getPosition(playerFields[0]);
					String name = playerFields[1];
					int cost = Integer.parseInt(playerFields[2]);
					String gameInfo = playerFields[3];
					double avgPPG = Double.parseDouble(playerFields[4]);
					String teamAbbrev = playerFields[5];
					players.add(new Player(iPlayer++, name, position, cost, 20, avgPPG, gameInfo, teamAbbrev));
				}
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		return players;
	}
	@Override public int getSalary() {
		return SALARY;
	}
	@Override public ArrayList<Position> getRosterPositions() {
		return rosterPositions;
	}
	@Override public ArrayList<FlexPosition> getRosterFlexPositions() {
		return rosterFlexPositions;
	}
	@Override public int getMinCostForPosition(Position position) {
		if (position == FPosition.QB) return 5000;
		if (position == FPosition.RB) return 3000;
		if (position == FPosition.WR) return 3000;
		if (position == FPosition.TE) return 2500;
		if (position == FPosition.DST) return 2000;
		return 0;
	}
	@Override public double getPtsForStat(Stat stat, double value) {
		FStat fStat = (FStat)stat;
		switch(fStat) {
			case PASS_TD: return value*4; 
			case PASS_YDS: return value/25 + (value >= 300 ? 3 : 0);
			case INT: return value*-1;
			case RUSH_YDS: return value/10 + (value >= 100 ? 3 : 0);
			case RUSH_TD: return value*6;
			case REC: return value;
			case REC_YDS: return value/10 + (value >= 100 ? 3 : 0);
			case REC_TD: return value*6;
			case MISC_TD: return value*6;
			case FUM: return value*-1;
			case CONV_2PT: return value*2;
			case SACK: return value;
			case DEF_INT: return value*2;
			case DEF_FUM: return value*2;
			case DEF_TD: return value*6;
			case SFTY: return value*2;
			case BLK: return value*2;
			case DEF_CONV_2PT: return value*2;
			case PTS_ALLOWED:
				if (value == 0) return 10;
				if (value <= 6) return 7;
				if (value <= 13) return 4;
				if (value <= 20) return 1;
				if (value <= 27) return 0;
				if (value <= 34) return -1;
				return -4;
			default: return 0;
		}
	}
}
