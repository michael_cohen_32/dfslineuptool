package football;

import shared.Stat;

public enum FStat implements Stat {
	PASS_TD, PASS_YDS, INT, RUSH_YDS, RUSH_TD, REC, REC_YDS, REC_TD, MISC_TD, FUM, CONV_2PT,
	SACK, DEF_INT, DEF_FUM, DEF_TD, SFTY, BLK, DEF_CONV_2PT, PTS_ALLOWED;
}
