package football;

import shared.Position;

public enum FPosition implements Position {
	QB, RB, WR, TE, K, DST;
	
	public static FPosition getPosition(String sPosition) {
		for (FPosition fPosition : FPosition.values()) {
			if (sPosition.equalsIgnoreCase(fPosition.name())) return fPosition;
		}
		return null;
	}
}
