package football;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import shared.DFSClassifier;
import shared.Position;
import shared.Stat;

/**
 * Get Football Projections from FantasyPros
 * @author Michael
 */
public class FProjections {
	private static final int WEEK = 7;
	private static final String PATH = "/Users/Michael/Documents/dfs_data/Projections/Week" + WEEK;	
	private static final String getPath(FPosition fPos) {
		return PATH + "/" + fPos.name() + ".csv";
	}
	
	private DFSClassifier dfsClassifier;
	private Map<String, Double> nameToProjPts = new HashMap<String, Double>();

	public FProjections(DFSClassifier dfsClassifier) {
		this.dfsClassifier = dfsClassifier;
		List<Position> positions = dfsClassifier.getRosterPositions();
		for (Position position : positions) {
			fetchProjections((FPosition)position);
		}
	}
	public double getProjectedPoints(String name) {
		return nameToProjPts.containsKey(name) ? nameToProjPts.get(name) : 0;
	}
	private void fetchProjections(FPosition fPos) {
		BufferedReader br = null; String line = ""; String csvSplitBy =",";
		try {
			br = new BufferedReader(new FileReader(getPath(fPos)));
			boolean first = false;
			while ((line = br.readLine()) != null) {
				line.replaceAll("\"", "");
				String[] fields = line.split(csvSplitBy);
				if (fields.length <= 2) continue;
				if (!first) {
					first = true;
					continue;
				} else {
					parseLineIntoProjection(fields, fPos);
				}
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private void parseLineIntoProjection(String[] fields, FPosition fPos) {
		switch(fPos) {
			case QB:
				parseLineQB(fields); break;
			case RB:
				parseLineRB(fields); break;
			case WR:
				parseLineWR(fields); break;
			case TE:
				parseLineTE(fields); break;
			case K:
				parseLineK(fields); break;
			case DST:
				parseLineDST(fields); break;
		}
	}
	private void parseLineQB(String[] fields) {
		String name = fields[0]; Double passYds = Double.parseDouble(fields[4]); Double passTDs = Double.parseDouble(fields[5]);
		Double ints = Double.parseDouble(fields[6]); Double rushYds = Double.parseDouble(fields[8]);
		Double rushTDs = Double.parseDouble(fields[9]); Double fumbles = Double.parseDouble(fields[10]);
		Map<Stat, Double> playerStats = new HashMap<Stat, Double>();
		playerStats.put(FStat.PASS_YDS, passYds); playerStats.put(FStat.PASS_TD, passTDs); playerStats.put(FStat.INT, ints);
		playerStats.put(FStat.RUSH_YDS, rushYds); playerStats.put(FStat.RUSH_TD, rushTDs); playerStats.put(FStat.FUM, fumbles);
		nameToProjPts.put(name, dfsClassifier.getPtsForStats(playerStats));
	}
	private void parseLineRB(String[] fields) {
		String name = fields[0]; Double rushYds = Double.parseDouble(fields[3]); Double rushTDs = Double.parseDouble(fields[4]);
		Double recs = Double.parseDouble(fields[5]); Double recYds = Double.parseDouble(fields[6]); Double recTDs = Double.parseDouble(fields[7]); Double fumbles = Double.parseDouble(fields[8]);
		Map<Stat, Double> playerStats = new HashMap<Stat, Double>();
		playerStats.put(FStat.RUSH_YDS, rushYds); playerStats.put(FStat.RUSH_TD, rushTDs); playerStats.put(FStat.REC, recs);
		playerStats.put(FStat.REC_YDS, recYds); playerStats.put(FStat.REC_TD, recTDs); playerStats.put(FStat.FUM, fumbles);
		nameToProjPts.put(name, dfsClassifier.getPtsForStats(playerStats));
	}
	private void parseLineWR(String[] fields) {
		String name = fields[0]; Double rushYds = Double.parseDouble(fields[3]); Double rushTDs = Double.parseDouble(fields[4]);
		Double recs = Double.parseDouble(fields[5]); Double recYds = Double.parseDouble(fields[6]); Double recTDs = Double.parseDouble(fields[7]); Double fumbles = Double.parseDouble(fields[8]);
		Map<Stat, Double> playerStats = new HashMap<Stat, Double>();
		playerStats.put(FStat.RUSH_YDS, rushYds); playerStats.put(FStat.RUSH_TD, rushTDs); playerStats.put(FStat.REC, recs);
		playerStats.put(FStat.REC_YDS, recYds); playerStats.put(FStat.REC_TD, recTDs); playerStats.put(FStat.FUM, fumbles);
		nameToProjPts.put(name, dfsClassifier.getPtsForStats(playerStats));
	}
	private void parseLineTE(String[] fields) {
		String name = fields[0]; 
		Double recs = Double.parseDouble(fields[2]); Double recYds = Double.parseDouble(fields[3]); Double recTDs = Double.parseDouble(fields[4]); Double fumbles = Double.parseDouble(fields[5]);
		Map<Stat, Double> playerStats = new HashMap<Stat, Double>();
		playerStats.put(FStat.REC, recs); playerStats.put(FStat.REC_YDS, recYds); playerStats.put(FStat.REC_TD, recTDs); playerStats.put(FStat.FUM, fumbles);
		nameToProjPts.put(name, dfsClassifier.getPtsForStats(playerStats));
	}
	private void parseLineK(String[] fields) {
		
	}
	private void parseLineDST(String[] fields) {
		String name = fields[0]; name.replaceAll("\"", ""); Double sacks = Double.parseDouble(fields[2]); Double defInt = Double.parseDouble(fields[3]); Double defFum = Double.parseDouble(fields[4]);
		Double defTD = Double.parseDouble(fields[6]); Double safety = Double.parseDouble(fields[8]); Double ptsAllowed = Double.parseDouble(fields[9]);
		Map<Stat, Double> playerStats = new HashMap<Stat, Double>();
		playerStats.put(FStat.SACK, sacks); playerStats.put(FStat.DEF_INT, defInt); playerStats.put(FStat.DEF_FUM, defFum);
		playerStats.put(FStat.DEF_TD, defTD); playerStats.put(FStat.SFTY, safety); playerStats.put(FStat.PTS_ALLOWED, ptsAllowed);
		nameToProjPts.put(name, dfsClassifier.getPtsForStats(playerStats));
	}
}
