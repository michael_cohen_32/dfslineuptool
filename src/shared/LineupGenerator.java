package shared;

import java.util.ArrayList;
import java.util.List;

public class LineupGenerator {
	private DFSClassifier dfsClassifier;
	private List<Lineup> validLineups = new ArrayList<Lineup>();
	private List<Integer> lockedInPlayers;
	private List<Integer> lockedOutPlayers;
	
	public LineupGenerator(DFSClassifier dfsClassifier, List<Integer> lockedInPlayers, List<Integer> lockedOutPlayers) {
		this.dfsClassifier = dfsClassifier; this.lockedInPlayers = lockedInPlayers; this.lockedOutPlayers = lockedOutPlayers;
		generateAllPossibleLineups();
	}
	private void generateAllPossibleLineups() {
		int numPlayers = dfsClassifier.getNumPlayers(); int rosterSize = dfsClassifier.getRosterSize();
		LineupStreamer lineupStreamer = new LineupStreamer(numPlayers, rosterSize, lockedInPlayers, lockedOutPlayers);
		List<Integer> iPlayers; boolean lastInvalid = false;
		while ((iPlayers = lineupStreamer.getNextLineup(lastInvalid)) != null) {
			Lineup lineup = new Lineup();
			for (Integer iPlayer : lockedInPlayers) lineup.addPlayer(dfsClassifier.getPlayer(iPlayer));
			for (Integer iPlayer : iPlayers) lineup.addPlayer(dfsClassifier.getPlayer(iPlayer));
			if (lineup.size() < rosterSize) lastInvalid = !isValidLineupSoFar(lineup);
			else if (lineup.size() == rosterSize) {
				if (isValidCompletedLineup(lineup)) {
					validLineups.add(lineup);
					System.out.println(lineup);
				}
				lastInvalid = false;
			} else {
				lastInvalid = true;
			}
		}
	}
	private boolean lineupContainsLockedOutPlayer(Lineup lineup) {
		for (Player player : lineup.getPlayers()) {
			if (lockedOutPlayers.contains(dfsClassifier.getPlayers().indexOf(player))) return true;
		}
		return false;
	}
	private boolean isValidLineupSoFar(Lineup lineup) {
		return !lineupContainsLockedOutPlayer(lineup) && Utils.isValidLineup(lineup, dfsClassifier, false);
	}
	private boolean isValidCompletedLineup(Lineup lineup) {
		return !lineupContainsLockedOutPlayer(lineup) && Utils.isValidLineup(lineup, dfsClassifier, true);
	}

	/**
	 * Helper class - generates index lists which represent lineups
	 * @author Michael
	 */
	private class LineupStreamer {
		private int numPlayers;
		private int lineupSize;
		private int curIdx = 0;
		private List<Integer> unusablePlayers;
		private List<Integer> iPlayers = new ArrayList<Integer>();
		/**
		 * @param numPlayers number of players in DFSClassifier
		 * @param lineupSize number of slots in lineup for DFSClassifier
		 * @param lockedInPlayers player indices who we want IN lineup
		 * @param lockedOutPlayers player indices who we want OUT of lineup
		 */
		private LineupStreamer(int numPlayers, int lineupSize, List<Integer> lockedInPlayers, List<Integer> lockedOutPlayers) {
			this.numPlayers = numPlayers; this.lineupSize = lineupSize - lockedInPlayers.size();
			unusablePlayers = new ArrayList<Integer>(); unusablePlayers.addAll(lockedInPlayers); unusablePlayers.addAll(lockedOutPlayers);
		}
		private List<Integer> getNextLineup(boolean lastInvalid) {
			return lastInvalid ? getNextLineupIfInvalid() : getNextLineup();
		}
		private List<Integer> getNextLineupIfInvalid() {
			if (iPlayers.size() == 0) {
				iPlayers.add(curIdx); curIdx++; return iPlayers;
			}
			if ((curIdx == numPlayers && numPlayers - curIdx <= lineupSize - iPlayers.size()) || (curIdx < numPlayers && numPlayers - curIdx < lineupSize - iPlayers.size())) {
				int numToAvoid = curIdx-1; int idx = iPlayers.size()-1;
				while (iPlayers.get(idx) == numToAvoid) {
					numToAvoid--; idx--;
					if (idx < 0) return null;
				}
				int newValue = iPlayers.get(idx)+1; iPlayers.set(idx, newValue);
				for (int i = iPlayers.size()-1; i > idx; i--) {
					iPlayers.remove(i);
				}
				curIdx = newValue+1;
				return iPlayers;
			}
			iPlayers.set(iPlayers.size()-1, curIdx);
			curIdx++;
			return iPlayers;
		}
		private List<Integer> getNextLineup() {
			if (iPlayers.size() == lineupSize) { //if we can't add any more new players
				if (curIdx == numPlayers) { //if we've reached the end of the player list (and need to propagate backward)
					int numToAvoid = curIdx-1;
					int idx = lineupSize-1;
					while (iPlayers.get(idx) == numToAvoid) { //figure out how far we have to propogate back
						numToAvoid--;
						idx--;
						if (idx < 0) return null; //if we propogate back all the way and still can't do anything, no more lineups can be made
					}
					int newValue = iPlayers.get(idx)+1;
					iPlayers.set(idx, newValue);
					for (int i = iPlayers.size()-1; i > idx; i--) { //once we propogate back to a point, start fresh (delete indices in after that point)
						iPlayers.remove(i);
					}
					curIdx = newValue+1;
				} else {
					iPlayers.set(lineupSize-1, curIdx);
					curIdx++;
				}
			} else {
				if (numPlayers - curIdx < lineupSize - iPlayers.size()) {
					int numToAvoid = curIdx-1; int idx = iPlayers.size()-1;
					while (iPlayers.get(idx) == numToAvoid) {
						numToAvoid--; idx--;
						if (idx < 0) return null;
					}
					int newValue = iPlayers.get(idx)+1; iPlayers.set(idx, newValue);
					for (int i = iPlayers.size()-1; i > idx; i--) {
						iPlayers.remove(i);
					}
					curIdx = newValue+1;
					return iPlayers;
				}
				iPlayers.add(curIdx);
				curIdx++;
			}
			return iPlayers;
		}
	}
}