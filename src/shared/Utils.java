package shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class Utils {

	public static ArrayList<Lineup> sortLineups(ArrayList<Lineup> lineups) {return sortLineups(lineups, false);}
	/**
	 * @param inc true if increasing, false if decreasing
	 * @return sorted lineup
	 */
	public static ArrayList<Lineup> sortLineups(ArrayList<Lineup> lineups, boolean inc) {
		Collections.sort(lineups, new Comparator<Lineup>() {
			@Override public int compare(Lineup l1, Lineup l2) {
				int l1Pts = l1.getProjectedPoints(); int l2Pts = l2.getProjectedPoints();
				if (l1Pts == l2Pts) return 0;
				if (l1Pts > l2Pts) return inc ? -1 : 1;
				return inc ? 1 : -1;
			}
		});
		return lineups;
	}
	public static double getPointsForStats(DFSClassifier dfsClassifier, Map<Stat, Double> statValues) {
		return dfsClassifier.getPtsForStats(statValues);
	}
	public static boolean isValidLineup(Lineup lineup, DFSClassifier dfsClassifier, boolean checkForComplete) {
		return lineupHasValidCost(lineup, dfsClassifier.getSalary()) && lineupHasValidPositions(lineup, dfsClassifier.getRosterPositions(), dfsClassifier.getRosterFlexPositions(), checkForComplete);
	}
	public static boolean lineupHasValidCost(Lineup lineup, int salary) {
		return lineup.getCost() <= salary;
	}
	public static boolean lineupHasValidPositions(Lineup lineup, ArrayList<Position> positions, ArrayList<FlexPosition> flexPositions, boolean checkForComplete) {
		ArrayList<Position> rosterPositions = new ArrayList<Position>();
		for (Position position : positions) rosterPositions.add(position);
		ArrayList<Position> leftoverForFlex = new ArrayList<Position>();
		for (Player player : lineup.getPlayers()) {
			Position playerPosition = player.getPosition();
			int idx = rosterPositions.indexOf(playerPosition);
			if (idx == -1) leftoverForFlex.add(playerPosition);
			else rosterPositions.remove(idx);
		}
		return (!checkForComplete || rosterPositions.size() == 0) && lineupHasValidFlexPositions(leftoverForFlex, flexPositions, checkForComplete);
	}
	private static boolean lineupHasValidFlexPositions(ArrayList<Position> leftoverPositions, ArrayList<FlexPosition> rosterFlexPositions, boolean checkForComplete) {
		List<Position> positions = new ArrayList<Position>(); List<FlexPosition> flexPositions = new ArrayList<FlexPosition>();
		for (Position position : leftoverPositions) positions.add(position);
		for (FlexPosition flexPosition : rosterFlexPositions) flexPositions.add(flexPosition);
		return lineupHasValidFlexPositionsHelper(positions, flexPositions, checkForComplete);
	}
	private static boolean lineupHasValidFlexPositionsHelper(List<Position> positions, List<FlexPosition> flexPositions, boolean checkForComplete) {
		if (positions.size() == 0) return !checkForComplete || flexPositions.size() == 0;
		Position position = positions.get(0);
		for (int i = 0; i < flexPositions.size(); i++) {
			FlexPosition flexPosition = flexPositions.get(i);
			if (flexPosition.isValidPosition(position)) {
				ArrayList<Position> newPositions = new ArrayList<Position>();
				for (int j = 0; j < positions.size(); j++) newPositions.add(positions.get(j));
				newPositions.remove(position);
				ArrayList<FlexPosition> newFlexPositions = new ArrayList<FlexPosition>();
				for (int j = 0; j < flexPositions.size(); j++) newFlexPositions.add(flexPositions.get(j));
				newFlexPositions.remove(flexPosition);
				if (lineupHasValidFlexPositionsHelper(newPositions, newFlexPositions, checkForComplete)) {
					return true;
				}
			}
		}
		return false;
	}
}
