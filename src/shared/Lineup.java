package shared;

import java.util.ArrayList;
import java.util.List;

//TODO: consider making lineup contain List<Integer> iPlayers
public class Lineup {
	private List<Player> players = new ArrayList<Player>();

	public Lineup() {}
	public int size() {return players.size();}
	public void addPlayer(Player player) {players.add(player);}
	public void removePlayer(int idx) {players.remove(idx);}
	public int getCost() {
		int cost = 0;
		for (Player player : players) {
			cost += player.getCost();
		}
		return cost;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public int getProjectedPoints() {
		int projectedPts = 0;
		for (Player player : players) {
			projectedPts += player.getProjectedPts();
		}
		return projectedPts;
	}

	public String toString() { return players.toString(); }
}
