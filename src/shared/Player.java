package shared;

public class Player {
	private int id;
	private String name;
	private Position position;
	private int cost;
	private double projectedPts;
	
	private Double avgPPG;
	private String gameInfo;
	private String teamAbbrev;
	
	//simplified constructor if we don't care about the other info
	public Player(int id, String name, Position position, int cost, double projectedPts) {
		this(id, name, position, cost, projectedPts, null, null, null);
	}
	public Player(int id, String name, Position position, int cost, double projectedPts, Double avgPPG, String gameInfo, String teamAbbrev) {
		this.id = id; this.name = name; this.position = position; this.cost = cost; this.projectedPts = projectedPts; this.avgPPG = avgPPG; this.gameInfo = gameInfo; this.teamAbbrev = teamAbbrev;
	}
	public int getId() { return id; }
	public String getName() { return name; }
	public Position getPosition() { return position; }
	public int getCost() { return cost; }
	public double getProjectedPts() { return projectedPts; }
	public Double getAvgPPG() { return avgPPG; }
	public String getGameInfo() { return gameInfo; }
	public String getTeamAbbrev() { return teamAbbrev; }
	
	public String toString() { return position + ": " + name + " (" + cost + ")"; }
}