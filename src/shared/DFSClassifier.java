package shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Store players in position buckets
 * @author Michael
 */
public abstract class DFSClassifier {
	private Map<Position, ArrayList<Player>> playersByPosition = new HashMap<Position, ArrayList<Player>>();
	private List<Player> players = new ArrayList<Player>();
	
	public DFSClassifier() {
		ArrayList<Player> players = loadPlayers();
		orderPlayersByPosition(players);
	}
	public int getNumPlayersForPosition(Position position) {return playersByPosition.containsKey(position) ? playersByPosition.get(position).size() : 0;}
	public Player getPlayerAtFlexPositionIndex(int idx, FlexPosition flexPosition) {
		if (!getRosterFlexPositions().contains(flexPosition)) return null;
		List<Position> positions = new ArrayList<Position>(); positions.addAll(flexPosition.getValidPositions());
		Collections.sort(positions, new Comparator<Position>() {
			@Override public int compare(Position p1, Position p2) {
				int idx1 = getRosterPositions().indexOf(p1); int idx2 = getRosterPositions().indexOf(p2);
				return idx1-idx2;
			}
		});
		for (int i = 0; i < positions.size(); i++) {
			Position position = positions.get(i);
			int numPlayers = getNumPlayersForPosition(position);
			if (idx < numPlayers) return getPlayerAtPositionIndex(idx, position);
			idx -= numPlayers;
		}
		return null;
	}
	public Player getPlayerAtPositionIndex(int idx, Position position) {
		if (!playersByPosition.containsKey(position)) return null;
		int numPlayersForPosition = getNumPlayersForPosition(position);
		return idx >= 0 && idx < numPlayersForPosition ? playersByPosition.get(position).get(idx) : null;
	}
	public List<Player> getPlayers() {
		List<Player> players = new ArrayList<Player>();
		for (Position position : playersByPosition.keySet()) {
			players.addAll(playersByPosition.get(position));
		}
		return players;
	}
	public Player getPlayer(int idx) {
		return idx >= 0 && idx < players.size() ? players.get(idx) : null;
	}
	public int getNumPlayers() {return players.size();}
	public int getRosterSize() {return getRosterPositions().size() + getRosterFlexPositions().size();}
	
	public abstract int getSalary();
	public abstract ArrayList<Position> getRosterPositions();
	public abstract ArrayList<FlexPosition> getRosterFlexPositions();
	public abstract ArrayList<Player> loadPlayers();
	/** Order players by position (ex: for football, QB first) and within position, order by cost **/
	public abstract int getMinCostForPosition(Position position);
	public abstract double getPtsForStat(Stat stat, double value);
	public double getPtsForStats(Map<Stat, Double> stats) {
		double pts = 0;
		for (Stat stat : stats.keySet()) {
			Double value = stats.get(stat);
			pts += getPtsForStat(stat, value);
		}
		return pts;
	}
	private void orderPlayersByPosition(ArrayList<Player> players) {
		playersByPosition.clear();
		ArrayList<Position> rosterPositions = getRosterPositions();
		for (Position position : rosterPositions) playersByPosition.put(position, new ArrayList<Player>());
		for (Player player : players) playersByPosition.get(player.getPosition()).add(player);
		this.players.clear();
		for (Position position : playersByPosition.keySet()) {
			this.players.addAll(playersByPosition.get(position));
		}
	}
	public int getMinCostForFlexPosition(FlexPosition flexPosition) {
		int minCost = Integer.MAX_VALUE;
		for (Position position : flexPosition.getValidPositions()) {
			int minPositionCost = getMinCostForPosition(position);
			if (minPositionCost < minCost) minCost = minPositionCost;
		}
		return minCost;
	}
	public int getMinCostForPositionList(List<Position> positions) {
		int minCost = 0;
		for (Position position : positions) minCost += getMinCostForPosition(position);
		return minCost;
	}
	public int getMinCostForFlexPositionList(List<FlexPosition> flexPositions) {
		int minCost = 0;
		for (FlexPosition flexPosition : flexPositions) minCost += getMinCostForFlexPosition(flexPosition);
		return minCost;
	}
	/** Filter out if 1) there is a cheaper option with more projected points or 2) projected pts = 0 **//*
	private ArrayList<Player> filterUnnecessaryPlayers(ArrayList<Player> players) {
		List<Player> playersToRemove = new ArrayList<Player>();
		for (Player player : players) {
			if (shouldFilterOutPlayer(player, players)) playersToRemove.add(player);
		}
		for (Player player : playersToRemove) {
			int idx = players.indexOf(player);
			if (idx != -1) players.remove(player);
		}
		return players;
	}
	private boolean shouldFilterOutPlayer(Player player, ArrayList<Player> players) {
		Position position = player.getPosition(); int cost = player.getCost(); double projPts = player.getProjectedPts();
		if (projPts == 0) return true;
		for (Player p : players) {
			if (p.getPosition() == position && p.getCost() < cost && p.getProjectedPts() > projPts) return true;
		}
		return false;
	}*/
}
