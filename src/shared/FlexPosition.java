package shared;

import java.util.Arrays;
import java.util.List;

public class FlexPosition {
	private List<Position> validPositions;
	public FlexPosition(Position... validPositions) {
		this(Arrays.asList(validPositions));
	}
	public FlexPosition(List<Position> validPositions) {
		this.validPositions = validPositions;
	}
	public List<Position> getValidPositions() {
		return validPositions;
	}
	public boolean isValidPosition(Position position) {
		return validPositions.contains(position);
	}
}
